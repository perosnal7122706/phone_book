#include <grpcpp/grpcpp.h>
#include "sqlite3/sqlite3.h"
#include "phone_book.grpc.pb.h"
#include <string>

#define DB_NAME "phone_book.db"

using std::string;
using std::cout;
using grpc::Server;
using grpc::ServerUnaryReactor;
using grpc::ServerWriteReactor;
using grpc::CallbackServerContext;
using grpc::ServerBuilder;
using grpc::Status;
using grpc::StatusCode;
using phonebook::PhoneBook;
using phonebook::Record;
using phonebook::Response;


class PhoneBookImpl final: public PhoneBook::CallbackService{
public:
    ServerUnaryReactor* CreateRecord(CallbackServerContext* context, const Record* request, Record* reply) override {
        ServerUnaryReactor* reactor = context->DefaultReactor();

        sqlite3* db;
        int resp = sqlite3_open(DB_NAME, &db);
        if(resp!=SQLITE_OK){
            reactor->Finish(Status(StatusCode::INTERNAL, "DB"));
            return reactor;
        }

        string com=string("','");
        string val=string("('")+request->phone_number()+com+request->first_name()+com+
                request->second_name()+com+request->surname()+com+request->note()+string("');"); //prepare data for insertion
        char* err;
        resp = sqlite3_exec(db, (string ("INSERT INTO phones VALUES")+val).c_str(), 0, 0, &err);
        if (resp!=SQLITE_OK){
            reactor->Finish(Status(StatusCode::INTERNAL, "DB: "+string(err)));
            return reactor;
        }

        sqlite3_close(db);

        reply->CopyFrom(*request);
        reactor->Finish(Status::OK);
        return reactor;
    }

//    virtual ::grpc::ServerWriteReactor< ::phonebook::Record>* GetRecords(
//            ::grpc::CallbackServerContext* /*context*/, const ::phonebook::Record* /*request*/)  { return nullptr; }

    ServerWriteReactor<Record>* GetRecords(CallbackServerContext* context, const Record* request) override {
        class Lister : public ServerWriteReactor<Record> {
        public:
            Lister(const Record* request){
                record_list_=std::vector<Record>();

                int cond_count=0;
                string cond=string();
                if (!request->phone_number().empty()){
                    cond_count++;
                    cond+="number='"+request->phone_number()+"' ";
                }
                if (!request->first_name().empty()){
                    if(cond_count!=0){
                        cond+="AND ";
                    }
                    cond_count++;
                    cond+="first_name='"+request->first_name()+"' ";
                }
                if (!request->second_name().empty()){
                    if(cond_count!=0){
                        cond+="AND ";
                    }
                    cond_count++;
                    cond+="second_name='"+request->second_name()+"' ";
                }
                if (!request->surname().empty()){
                    if(cond_count!=0){
                        cond+="AND ";
                    }
                    cond_count++;
                    cond+="surname='"+request->surname()+"' ";
                }
                if (!request->note().empty()){
                    if(cond_count!=0){
                        cond+="AND ";
                    }
                    cond_count++;
                    cond+="note='"+request->note()+"' ";
                }

                if (cond_count!=0){
                    sqlite3* db;
                    sqlite3_stmt* stmt;
                    int resp = sqlite3_open(DB_NAME, &db);
                    if(resp!=SQLITE_OK){
                        Finish(Status(StatusCode::INTERNAL, "DB"));
                    }

                    resp = sqlite3_prepare_v2(db, string("SELECT * FROM phones WHERE " + cond + ";").c_str(), -1, &stmt, 0);
                    if (resp != SQLITE_OK) {
                        Finish(Status(StatusCode::INTERNAL, "DB"));
                    }

                    while(sqlite3_step(stmt)!=SQLITE_DONE){
                        Record rec=Record();
                        rec.set_phone_number(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0)));
                        rec.set_first_name(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 1)));
                        rec.set_second_name(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 2)));
                        rec.set_surname(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 3)));
                        rec.set_note(reinterpret_cast<const char*>(sqlite3_column_text(stmt, 4)));
                        record_list_.push_back(rec);
                    }

                    sqlite3_close(db);
                }
                next_record_=record_list_.begin();
                NextWrite();
            }
            void OnDone() override { delete this; }
            void OnWriteDone(bool ok) override { NextWrite(); }

        private:
            void NextWrite() {
                while (next_record_ != record_list_.end()) {
                    const Record& f = *next_record_;
                    next_record_++;

                    StartWrite(&f);
                    return;
                }
                // Didn't write anything, all is done.
                Finish(Status::OK);
            }
            std::vector<Record> record_list_;
            std::vector<Record>::const_iterator next_record_;
        };
//        record_list_=std::vector<Record>(10);
//        for(int i=0; i<record_list_.size(); i++){
//            record_list_[i]=Record();
//            record_list_[i].set_first_name("Test");
//            record_list_[i].set_second_name("User No "+std::to_string(i));
//            record_list_[i].set_phone_number("8-800-555-35-35");
//            record_list_[i].set_note("I'm just a dummy user");
//        }
        return new Lister(request);
    }


    ServerUnaryReactor* DeleteRecord(CallbackServerContext* context, const Record* request, Response* reply) override {

        auto* reactor = context->DefaultReactor();

        sqlite3* db;
        int resp = sqlite3_open(DB_NAME, &db);
        if(resp!=SQLITE_OK){
            reactor->Finish(Status(StatusCode::INTERNAL, "DB"));
            return reactor;
        }

        string val=string("'")+request->phone_number()+string("';"); //prepare data for deletion
        char* err;
        resp = sqlite3_exec(db, (string("DELETE FROM phones WHERE number=")+val).c_str(), 0, 0, &err);
        if (resp!=SQLITE_OK){
            reactor->Finish(Status(StatusCode::INTERNAL, "DB: "+string(err)));
            return reactor;
        }

        sqlite3_close(db);

        reply->set_data("Record deleted");
        reactor->Finish(Status::OK);
        return reactor;
    }
};

void run(){
    std::string addr("0.0.0.0:10024");
    PhoneBookImpl service;

    ServerBuilder builder;
    builder.AddListeningPort(addr, grpc::InsecureServerCredentials());
    builder.RegisterService(&service);

    sqlite3* db;
    int resp = sqlite3_open(DB_NAME, &db);
    if(resp!=SQLITE_OK){
        cout<<"Database opening error";
        return;
    }

    char* err;
    resp = sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS phones ("
                     "number VARCHAR(18) PRIMARY KEY,"   //'+' + country_code + operator_code + 3 + 2 + 2 + possible delimiters
                     "first_name VARCHAR(32),"
                     "second_name VARCHAR(32),"
                     "surname VARCHAR(32),"
                     "note VARCHAR(64));", 0, 0, &err);
    if (resp!=SQLITE_OK){
        cout<<"Database query error"<<err;
        return;
    }

    sqlite3_close(db);

    std::unique_ptr<Server> server(builder.BuildAndStart());
    cout<<"Server working at port: "<<addr<<"\n";
    server->Wait();
}


int main(){
    run();

    return 0;
}

