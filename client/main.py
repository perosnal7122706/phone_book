import tkinter as tk
import grpc
import phone_book_pb2 as pb
import phone_book_pb2_grpc as pb_grpc


def create_record(stub: pb_grpc.PhoneBookStub, record: pb.Record):
    return stub.CreateRecord(record)


def get_records(stub: pb_grpc.PhoneBookStub, record: pb.Record):
    return stub.GetRecords(record)


def delete_record(stub: pb_grpc.PhoneBookStub, record: pb.Record):
    return stub.DeleteRecord(record)


class App:

    def __init__(self):

        self.root = tk.Tk()
        self.frame = tk.Frame(self.root)

        self.label_texts = ["First name", "Second name", "Surname", "Phone number", "Note"]
        self.button_texts = ["Create", "Get", "Delete"]
        self.button_handlers = [self.create_handler, self.get_handler, self.delete_handler]

        # Create form for data insertion
        self.forms = [tk.Entry(self.frame) for i in range(len(self.label_texts))]
        for i in range(len(self.forms)):
            self.forms[i].grid(row=i, column=3, columnspan=3)

        # Create labels fr data forms
        self.labels = [tk.Label(self.frame, text=self.label_texts[i]) for i in range(len(self.label_texts))]
        for i in range(len(self.labels)):
            self.labels[i].grid(row=i, column=0, columnspan=3, stick="w")

        # Create buttons
        self.buttons = [tk.Button(self.frame, text=self.button_texts[i],
                                  command=self.button_handlers[i]) for i in range(len(self.button_texts))]
        for i in range(len(self.button_texts)):
            self.buttons[i].grid(row=len(self.label_texts), column=i * 2, columnspan=2)

        # Create status field
        self.status_field = tk.Label(self.frame)
        self.status_field.grid(row=len(self.label_texts) + 1, column=0, columnspan=6)
        self.status_field['text'] = "Response status here"

        # Create response field
        self.response_field = tk.Label(self.frame)
        self.response_field.grid(row=len(self.label_texts) + 2, column=0, columnspan=6, stick="w")
        self.response_field['text'] = "Loren ipsum ..."

        self.frame.pack()

    def __create_record__(self) -> pb.Record:
        return pb.Record(first_name=self.forms[0].get(), second_name=self.forms[1].get(),
                         surname=self.forms[2].get(), phone_number=self.forms[3].get(),
                         note=self.forms[4].get())

    def run(self):
        self.root.mainloop()

    def create_handler(self):
        record = self.__create_record__()
        response = ...
        if record.phone_number == "":
            self.response_field["text"] = "No phone number provided!"
            self.status_field["text"] = "ERROR"
            return
        with grpc.insecure_channel("localhost:10024") as channel:
            stub = pb_grpc.PhoneBookStub(channel)
            response = create_record(stub, record)
        self.response_field["text"] = response.__str__()
        self.status_field["text"] = "CREATED"

    def get_handler(self):
        record = self.__create_record__()
        response = ...
        text = ""
        with grpc.insecure_channel("localhost:10024") as channel:
            stub = pb_grpc.PhoneBookStub(channel)
            response = get_records(stub, record)
            for resp in response:
                text += "==========Record=========\n"
                text += resp.__str__()
        self.response_field["text"] = text
        self.status_field["text"] = "RETRIEVED"

    def delete_handler(self):
        record = self.__create_record__()
        response = ...
        with grpc.insecure_channel("localhost:10024") as channel:
            stub = pb_grpc.PhoneBookStub(channel)
            response = delete_record(stub, record)
        self.response_field["text"] = ""
        self.status_field["text"] = "DELETED"


if __name__ == "__main__":
    app = App()
    app.run()
