#!/usr/bin/env bash

python -m grpc_tools.protoc -I../.. --python_out=.. --pyi_out=.. --grpc_python_out=.. ../../phone_book.proto